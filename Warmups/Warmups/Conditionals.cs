﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Net;
using System.Runtime.Remoting.Messaging;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Warmups
{
    public class Conditionals
    {
        /// <summary>
        /// Exercise 1 - MischeviousChildren
        /// </summary>
        /// <param name="ASmile"></param>
        /// <param name="bSmile"></param>
        /// <returns></returns>
        public bool AreWeInTrouble(bool aSmile, bool bSmile)
        {
            if (aSmile == bSmile)
            {
                return true;
            }

            else
            {
                return false;
            }

        }
        /// <summary>
        /// Ex.#2
        /// </summary>
        /// <param name="vaca"></param>
        /// <param name="weekday"></param>
        /// <returns></returns>
        public bool CanSleepIn(bool vaca, bool weekday)
        {
            if ( vaca == true)
            {
                return true;
            }
            return vaca == weekday;
        }

        /// <summary>
        /// Exercise #3
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public int SumDouble(int a, int b)
        {
            if (a == b)
            {
                return (a + b) * 2;
            }
            return a + b;
        }

        /// <summary>
        /// Exercise #4
        /// </summary>
        /// <param name="n"></param>
        /// <returns></returns>
        public int Diff21(int n)
        {
            if (n <= 21)
            {
                return Math.Abs(n - 21);
            }
            return Math.Abs(n - 21) * 2;
        }

        /// <summary>
        /// Exercise #5
        /// </summary>
        /// <param name="isTalking"></param>
        /// <param name="hour"></param>
        /// <returns></returns>
        public bool ParrotTrouble(bool isTalking, int hour)
        {
            if (isTalking && (hour < 7 || hour >= 20))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// Ex.#6
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public bool Makes10(int a, int b)
        {
            if (a == 10 || b == 10)
            {
                return true;
            }
            if (a + b == 10)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Exercise #7
        /// </summary>
        /// <param name="n"></param>
        /// <returns></returns>
        public bool NearHundred(int n)
        {
            if ((n >= 90 && n <= 110) || (n >= 190 && n <= 210))
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        /// <summary>
        /// Exercise #8
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="negative"></param>
        /// <returns></returns>
        public bool PosNeg(int a, int b, bool negative)
        {
            if (a < 0 && b > 0 && negative == false)
            {
                return true;
            }
            if (a > 0 && b < 0 && negative == false)
            {
                return true;
            }
            if (a < 0 && b < 0 && negative == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Exercise #9
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public string NotString(string s)
        {
            if (s.IndexOf("not") == 0)
            {
                return s;
            }
            else
            {
                return "not " + s;
            }
        }

        /// <summary>
        /// Exercise #10
        /// </summary>
        /// <param name="str"></param>
        /// <param name="n"></param>
        /// <returns></returns>
        public string MissingChar(string str, int n)
        {
            //string a = str.Substring(n, n + 1);
            //string b = str.Substring(0, n);
            //string c = str.Substring(n + 1);
            //{
            return str.Remove(n, 1);
            //}
        }

        /// <summary>
        /// Exercise #11
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public string FrontBack(string str)
        {
            return str.Last() + str.Substring(1, str.Length - 2) + str.First();
        }
        /// <summary>
        /// Exercise #12
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public string Front3(string str)
        {
            if (str.Length < 3)
            {
                return str + str + str;
            }
            else
            {
                return str.Substring(0, 3) + str.Substring(0, 3) + str.Substring(0, 3);
            }
        }
        /// <summary>
        /// Exercise #13
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public string BackAround(string str)
        {
            char LastLetter = str.Last();

            return LastLetter + str + LastLetter;
        }

        /// <summary>
        /// Exercise #14
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public bool Multiple3or5(int number)
        {
            if ((number % 3) == 0)
            {
                return true;
            }
            else if ((number % 5) == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Exercise #15
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public bool StartHi(string str)
        {
            if (str.Substring(0, 3) == "hi ")
            {
                return true;
            }
                return false;
        }
        /// <summary>
        /// Ex.#16
        /// </summary>
        /// <param name="temp1"></param>
        /// <param name="temp2"></param>
        /// <returns></returns>
        public bool IcyHot(int temp1, int temp2)
        {
            if (((temp1 < 0) && (temp2 > 100)) || ((temp1 > 100) && (temp2 < 0)))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Exercise #17
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public bool Between10and20(int a, int b)
        {
            if ((a >= 10 && a <= 20) || (b >= 10 && b <= 20))
            {
                return true;
            }
            else
            {
                return false;

            }
        }
        /// <summary>
        /// Ex.#18
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        /// <returns></returns>
        public bool HasTeen(int a, int b, int c)
        {
            if ((a >= 13 && a <= 19) || (b >= 13 && b <= 19) || (c >= 13 && c <= 19))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Exercise #19
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public bool SoAlone(int a, int b)
        {
            if ((a >= 13 && a <= 19) && (b >= 13 && b <= 19))
            {
                return false;
            }
            else if ((a >= 13 && a <= 19) || (b >= 13 && b <= 19))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// Ex.#20
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public string RemoveDel(string str)
        {
            if (str.Contains("del") && str.IndexOf("del") == 1)
            {
                str = str.Remove(1, 3);
            }
            return str;
        }

        /// <summary>
        /// Exercise #21
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public bool IxStart(string str)
        {
            if (str.Substring(1, 9) == "ix snacks")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// Ex.#22
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public string StartOz(string str)
        {
            if ((str.StartsWith("o")) && (str.Substring(1,1) == "z"))
            {
                str = "oz";
                return str;
            }
            if (str.Substring(1,1) == "z")
            {
                str = "z";
                return str;
            }
            return null;
        }

        /// <summary>
        /// Exercise #23
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        /// <returns></returns>
        public int Max(int a, int b, int c)
        {
            //take 3 values and compaire them what is the largest

            if ((a > b) && (a > c))
            {
                return a;
            }
            else if ((b > a) && (b > c))
            {
                return b;
            }
            else
            {
                return c;
            }
        }
        /// <summary>
        /// Ex.#24
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public int Closer(int a, int b)
        {
            int aDiff = Math.Abs(10 - a);
            int bDiff = Math.Abs(10 - b);

            if (aDiff == bDiff)
            {
                return 0;
            }

            else if (aDiff < bDiff)
            {
                return a;
            }

            return b;

        }

        /// <summary>
        /// Exercise #25
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public bool GotE(string str)
        {
            int i = 0;
            foreach (char e in str)
            {
                if (e == 'e')
                {
                    i++;
                }
                if ((i >= 1 && i <= 3))
                {
                    return true;
                }
            }
            return false;
        }
        /// <summary>
        /// Ex.#26
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public string EndUp(string str)
        {
            if (str.Length < 3)
            {
                str = str.ToUpper();
                return str;
            }
            int length = str.Length;
            int takeAway = length - 3;

            string upStr = str.Substring(length - 3);
            upStr = upStr.ToUpper();
            string restStr = str.Substring(0, takeAway);
            return restStr + upStr;
        }
        /// <summary>
        /// Ex.#27
        /// </summary>
        /// <param name="str"></param>
        /// <param name="n"></param>
        /// <returns></returns>
        public string EveryNth(string str, int n)
        {
            string tempstr = "";
            for (int i = 0; i < str.Length; i += n)
            {
                tempstr += str[i];
            }

            return tempstr;
        }
    }
         
    }






