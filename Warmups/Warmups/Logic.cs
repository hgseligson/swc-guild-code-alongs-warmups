﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warmups
{
    public class Logic
    {
        /// <summary>
        /// Ex. #1
        /// </summary>
        /// <param name="cigars"></param>
        /// <param name="isWeekend"></param>
        /// <returns></returns>
        /// When squirrels get together for a party, they like to have cigars. 
        /// A squirrel party is successful when the number of cigars is between 40 and 60, inclusive. 
        /// Unless it is the weekend, in which case there is no upper bound on the number of cigars. Return true if the party with the given values is successful, or false otherwise. 
        public bool GreatParty(int cigars, bool isWeekend)
        {
            return isWeekend && cigars >= 40 || 
                !isWeekend && cigars >= 40 && cigars <= 60;
        }
        /// <summary>
        /// Ex.#2
        /// </summary>
        /// <param name="yourStyle"></param>
        /// <param name="dateStyle"></param>
        /// <returns></returns>
        public int CanHazTable(int yourStyle, int dateStyle)
        {
            if ((yourStyle >= 8) || (dateStyle >= 8))
            {
                return 2;
            }
            else if ((yourStyle <= 2) || (dateStyle <= 2))
            {
                return 0;
            }
            return 1;
        }

        /// <summary>
        /// Ex.#3 
        /// </summary>
        /// <param name = "temp" ></ param >
        /// < param name="isSummer"></param>
        /// <returns></returns>
        /// The children in Cleveland spend most of the day playing outside.
        /// In particular, they play if the temperature is between 60 and 90 (inclusive). 
        /// Unless it is summer, then the upper limit is 100 instead of 90. Given an int temperature and a bool isSummer, return true if the children play and false otherwise.
        public bool PlayOutside(int temp, bool isSummer)
        {
            int upperLimit = 90;
            if (isSummer)
            {
                upperLimit = 100;
            }

            return (temp >= 60) && (temp <= upperLimit);
        }
        /// <summary>
        /// Ex.#4
        /// </summary>
        /// <param name="speed"></param>
        /// <param name="isBirthday"></param>
        /// <returns></returns>
        public int CaughtSpeeding(int speed, bool isBirthday)
        {
            int speedLimitIncreaser = 0;
            if (isBirthday)
            {
                speedLimitIncreaser = 5;
            }

            if (speed <= 60 + speedLimitIncreaser)
            {
                return 0;
            }

            else if (speed > 80 + speedLimitIncreaser)
            {
                return 2;
            }

            else
            {
                return 1;
            }
        }

        /// <summary>
        /// Ex.#5
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        /// Given 2 ints, a and b, return their sum. 
        /// However, sums in the range 10..19 inclusive are forbidden, so in that case just return 20. 
        public int SkipSum(int a, int b)
        {
            int sum = a + b;

            if (sum >= 10 && sum <= 19)
            {
                return 20;
            }

            else
            {
                return sum;
            }
        }
        /// <summary>
        /// Ex.#6
        /// </summary>
        /// <param name="day"></param>
        /// <param name="vacation"></param>
        /// <returns></returns>
        public string AlarmClock(int day, bool vacation)
        {
            if (vacation == true)
            {
                if ((day >= 1) && (day <= 5))
                {

                    return "10:00";
                }
                return "off";
            }
            else if (vacation == false)
            {
                if ((day >= 1) && (day <= 5))
                {
                    return "7:00";
                }

                return "10:00";
            }
            return null;
        }
        /// <summary>
        /// Ex.#7
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public bool LoveSix(int a, int b)
        {
            if (a == 6 || b == 6 || (a + b) == 6)
            {
                return true;
            }

            else
            {
                return false;
            }
        }
        /// <summary>
        /// Ex.#8
        /// </summary>
        /// <param name="n"></param>
        /// <param name="outsideMode"></param>
        /// <returns></returns>
        public bool InRange(int n, bool outsideMode)
        {
            if ((n >= 1) && (n <= 10) && (outsideMode == false))
            {
                return true;
            }
            else if (outsideMode == true)
            {
                if ((n <= 1) || (n >= 10))
                {
                    return true;
                }
                return false;
            }
            return false;
        }
        /// <summary>
        /// Ex.#9
        /// </summary>
        /// <param name="n"></param>
        /// <returns></returns>
        public bool SpecialEleven(int n)
        {
            if (n % 11 == 0 || n % 11 == 1)
            {
                return true;
            }

            else
            {
                return false;
            }
        }
        /// <summary>
        /// Ex.#10
        /// </summary>
        /// <param name="n"></param>
        /// <returns></returns>
        public bool Mod20(int n)
        {
            int x = n % 20;
            if ((x == 2) || (x == 1))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Ex.#11
        /// </summary>
        /// <param name="n"></param>
        /// <returns></returns>
        public bool Mod35(int n)
        {
            if (n % 3 == 0 && n % 5 == 0)
            {
                return false;
            }

            else if (n % 3 == 0 || n % 5 == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// Ex.#12
        /// </summary>
        /// <param name="isMorning"></param>
        /// <param name="isMom"></param>
        /// <param name="isAsleep"></param>
        /// <returns></returns>
        public bool AnswerCell(bool isMorning, bool isMom, bool isAsleep)
        {
            if (isAsleep == true)
            {
                return false;
            }
            if (isMom == true || isMorning == false)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Ex.#13
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        /// <returns></returns>
        public bool TwoIsOne(int a, int b, int c)
        {
            if (a + b == c || a + c == b || b + c == a)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        /// <summary>
        /// EX.#14
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        /// <param name="bOk"></param>
        /// <returns></returns>
        public bool AreInOrder(int a, int b, int c, bool bOk)
        {
            if ((b >= a) && (c >= b) && (bOk == false))
            {
                return true;
            }
            else if ((bOk == true) && (c > b))
            {
                return true;
            }
            return false;
        }
        /// <summary>
        /// Ex.#15
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        /// <param name="equalOk"></param>
        /// <returns></returns>
        public bool InOrderEqual(int a, int b, int c, bool equalOk)
        {
            if ((b > a && c > b) || (equalOk && b >= a && c >= b))
            {
                return true;
            }

            else
            {
                return false;
            }
        }

        /// <summary>
        /// Ex.#16
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        /// <returns></returns>
        public bool LastDigit(int a, int b, int c)
        {
            int newA = a % 10;
            int newB = b % 10;
            int newC = c % 10;

            if ((newA == newB) || (newA == newC) || (newB == newC))
            {
                return true;
            }
            return false;
        }
        /// <summary>
        /// Ex.#17
        /// </summary>
        /// <param name="die1"></param>
        /// <param name="die2"></param>
        /// <param name="noDoubles"></param>
        /// <returns></returns>
        public int RollDice(int die1, int die2, bool noDoubles)
        {
            if (noDoubles != true) return die1 + die2;

            if (die1 == die2)
            {
                return (die1 + die2 + 1);
            }
            return die1 + die2;
        }

    }
}

