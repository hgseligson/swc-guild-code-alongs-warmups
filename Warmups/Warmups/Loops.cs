﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace Warmups
{
    public class Loops
    {
        /// <summary>
        /// Ex.#1
        /// </summary>
        /// <param name="str"></param>
        /// <param name="n"></param>
        /// <returns></returns>
        public string StringTimes(string str, int n)
        {
            string message = "";
            for (int i = 0; i < n; i++)
            {
                message += str;
            }
            return message;
        }
        /// <summary>
        /// Ex.#2
        /// </summary>
        /// <param name="str"></param>
        /// <param name="n"></param>
        /// <returns></returns>
        public string FrontTimes(string str, int n)
        {
            if (str.Length <= 3)
            {
                string str2 = "";
                for (int i = 0; i < n; i++)
                {
                    str2 += str;
                }
                return str2;
            }
            else
            {
                string str2 = "";
                for (int i = 0; i < n; i++)
                {
                    str2 += str.Substring(0,3);
                }
                return str2;
            }
        }
        /// <summary>
        /// Ex.#3
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        /// Count the number of "xx" in the given string.
        /// We'll say that overlapping is allowed, so "xxx" contains 2 "xx". 
        public int CountXX(string str)
        {
            int num = 0;
            int x = 0;
            for (int i = 0; i < str.Length - 1 ; i++)
            {
                if (str.Substring(x, 2) == "xx")
                {
                    num ++;
                }
                x++;
            }
            return num;
        }
        /// <summary>
        /// Ex.#4
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public bool DoubleX(string str)
        {
            bool result = false;

            for (int i = 0; i < str.Length - 1; i++)
            {
                char r = str[i];
                char r2 = str[i + 1];
                if (r == 'x')
                {
                    if (r2 == 'x')
                    {
                        result = true;
                    }
                    break;

                }

            }

            return result;
        }

        /// <summary>
        /// Ex.#5
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public string EveryOther(string str)
        {
            string result = "";

          
            for(int i = 0; i < str.Length; i += 2)
            {
            
                result += str[i];
            }

            return result;
        }
        /// <summary>
        /// Ex.#6
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public string StringSplosion(string str)
        {
            string result = "";
            for (int i = 0; i < str.Length; i++)
            {
                result += str.Substring(0, i + 1);
            }
            return result;
        }
        /// <summary>
        /// Ex.#7
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        /// Given a string, return the count of the number of times that a substring length 2 appears in the string and also as the last 2 chars of the string, 
        /// so "hixxxhi" yields 1 (we won't count the end substring). 
        public int CountLast2(string str)
        {
            int count = 0;
            string strMatch = str.Substring(str.Length - 2, 2);
            for (int i = 0; i < str.Length - 2 - 1; i++)
            {
                if (str.Substring(i, 1) == strMatch.Substring(0, 1) &&
                    str.Substring(i + 1, 1) == strMatch.Substring(1, 1))
                {
                    count ++;
                }
            }
            return count;
        }
        /// <summary>
        /// Ex.#8
        /// </summary>
        /// <param name="numbers"></param>
        /// <returns></returns>
        public int Count9(int[] numbers)
        {
            int count = 0;
            for (int i = 0; i < numbers.Length; i++)
            {
                if (numbers[i] == 9)
                {
                    count++;
                }
            }
            return count;
        }


        /// <summary>
        /// Ex.#9
        /// </summary>
        /// <param name="numbers"></param>
        /// <returns></returns>
        /// Given an array of ints, return true if one of the first 4 elements in the array is a 9. 
        /// The array length may be less than 4. 
        public bool ArrayFront9(int[] numbers)
        {
            for (int i = 0; i < numbers.Length && i < 4; i++)
            {
                if (numbers[i] == 9)
                {
                    return true;
                }
            }

            return false;
        }
 
        /// <summary>
        /// Ex.#10
        /// </summary>
        /// <param name="numbers"></param>
        /// <returns></returns>
        public bool Array123(int[] numbers)
        {
            for (int i = 0; i < numbers.Length - 2; i++)
            {
                if (numbers[i] == 1 && numbers[i + 1] == 2 && numbers[i + 2] == 3)
                {
                    return true;
                }
            }
            return false;
        }
        /// <summary>
        /// Ex.#11
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public int SubStringMatch(string a, string b)
        {
            int count = 0;
            int maxLength = a.Length;
            if (b.Length < a.Length)
            {
                maxLength = b.Length;
            }
            for (int i = 0; i < maxLength - 1; i++)
            {
                if (a.Substring(i, 2) == b.Substring(i, 2))
                {
                    count++;
                }
            }
            return count;
        }

        /// <summary>
        /// Ex.#12
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public string StringX(string str)
        {
            string output = "";
            output += str[0];
            for (int i = 1; i < str.Length - 1; i++)
            {
                if (str[i] != 'x')
                {
                    output += str[i];
                }
            }
            output += str[str.Length - 1];
            return output;
        }

        /// <summary>
        /// Ex.#13
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public string AltPairs(string str)
        {
            string tempstr = "";

            for (int i = 0; i < str.Length; i += 4)
            {
                tempstr += str.Substring(i, 1);

                if (i + 1 < str.Length)
                {
                    tempstr += str.Substring(i + 1, 1);
                }
            }
            return tempstr;
        }
        /// <summary>
        /// Ex.#14
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public string DoNotYak(string str)
        {
            string output = str;
            output = output.Replace("yak", "");
            return output;
        }
        /// <summary>
        /// Ex.#15
        /// </summary>
        /// <param name="numbers"></param>
        /// <returns></returns>
        public int Array667(int[] numbers)
        {
            int count = 0;
            for (int i = 0; i < numbers.Length - 1; i++)
            {
                if (numbers[i] == 6)
                {
                    if (numbers[i + 1] == 6 || numbers[i + 1] == 7)
                    {
                        count++;
                    }
                }
            }
            return count;
        }
    }
}
