﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warmups
{
    public class Arrays
    {
        /// <summary>
        /// Ex.#1
        /// </summary>
        /// <param name="numbers"></param>
        /// <returns></returns>
        /// Given an array of ints, return true if 6 appears as either the first or last element in the array. 
        /// The array will be length 1 or more. 
        //public bool FirstLast6(int[] numbers)
        public bool FirstLast6(int[] numbers)
        {
            return numbers[0] == 6 || numbers[numbers.Length - 1] == 6;
        }
        /// <summary>
        /// Ex.#2
        /// </summary>
        /// <param name="numbers"></param>
        /// <returns></returns>
        public bool SameFirstLast(int[] numbers)
        {
            int length = numbers.Length;

            if ((length > 1) && (numbers.First() == numbers.Last()))
            {
                return true;
            }
            return false;
        }
        /// <summary>
        /// Ex.#3
        /// </summary>
        /// <param name="n"></param>
        /// <returns></returns>
        /// Return an int array length n containing the first n digits of pi.
        public int[] MakePi(int n)
        {

            string piStr = Math.PI.ToString();
            piStr = piStr.Remove(1, 1);
            char[] charArry = piStr.ToCharArray();
            int[] intArray = new int[n];

            for (int i = 0; i < n; i++)
            {
                intArray[i] = (int)(charArry[i]) - 48;
            }
            return intArray;

        }

        /// <summary>
        /// Ex.#4
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public bool commonEnd(int[] a, int[] b)
        {

            if ((a.First() == b.First()) || (a.Last() == b.Last()))
            {
                return true;
            }
            return false;
        }
        /// <summary>
        /// Ex.#5
        /// </summary>
        /// <param name="numbers"></param>
        /// <returns></returns>
        public int Sum(int[] numbers)
        {
            int sum = 0;

            foreach (var n in numbers)
            {
                sum += n;
            }
            return sum;
        }
        /// <summary>
        /// Ex.#6
        /// </summary>
        /// <param name="numbers"></param>
        /// <returns></returns>
        public int[] RotateLeft(int[] numbers)
        {
            int[] result = numbers;
            int firstNumber = numbers[0];
            for (int i = 1; i < numbers.Length; i++)
            {
                result[i - 1] = numbers[i];
            }
            result[result.Length - 1] = firstNumber;
            return result;
        }
        /// <summary>
        /// Ex.#7
        /// </summary>
        /// <param name="numbers"></param>
        /// <returns></returns>
        public int[] Reverse(int[] numbers)
        {
            int[] tempArry = new int[numbers.Length];
            for (int i = 0; i < numbers.Length; i++)
            {
                tempArry[i] = numbers[numbers.Length - (i + 1)];
            }
            return tempArry;
        }
        /// <summary>
        /// Ex.#8
        /// </summary>
        /// <param name="numbers"></param>
        /// <returns></returns>
        public int[] HigherWins(int[] numbers)
        {
            int num = 0;
            if (numbers.First() > numbers.Last())
            {
                num = numbers.First();
            }
            else
            {
                num = numbers.Last();
            }
            for (int i = 0; i < numbers.Length; i++)
            {
                numbers[i] = num;
            }
            return numbers;
        }
        /// <summary>
        /// Ex.#9
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public int[] GetMiddle(int[] a, int[] b)
        {
            int[] newArry = new int[2];

            newArry[0] = a[1];
            newArry[1] = b[1];
            return newArry;
        }
        /// <summary>
        /// Ex.#10
        /// </summary>
        /// <param name="numbers"></param>
        /// <returns></returns>
        public bool HasEven(int[] numbers)
        {

            for (int i = 0; i < numbers.Length; i++)
            {
                if (numbers[i] % 2 == 0)
                {
                    return true;
                }
                return false;
            }
            return false;
        }
        /// <summary>
        /// Ex.#11
        /// </summary>
        /// <param name="numbers"></param>
        /// <returns></returns>
        public int[] KeepLast(int[] numbers)
        {
            int[] tempArry = new int[numbers.Length * 2];
            tempArry[tempArry.Length - 1] = numbers[numbers.Length - 1];
            return tempArry;
        }   
         /// <summary>
         /// Ex.#12
         /// </summary>
         /// <param name="numbers"></param>
         /// <returns></returns>
        public bool Double23(int[] numbers)
        {
            if (numbers.Length > 0)
            {
                bool result = false;

                result = numbers.Where(num => num == 2).Count() == 2
                         || numbers.Where(num => num == 3).Count() == 2;
                return result;

            }
            return false;
        }
        /// <summary>
        /// Ex.#13
        /// </summary>
        /// <param name="numbers"></param>
        /// <returns></returns>
        public int[] Fix23(int[] numbers)
        {
            for (int i = 0; i < numbers.Length - 1; i++)
            {
                if (numbers[i] == 2 && numbers[i + 1] == 3)
                {
                    numbers[i + 1] = 0;
                }
            }
            return numbers;
        }
        /// <summary>
        /// Ex.#14
        /// </summary>
        /// <param name="numbers"></param>
        /// <returns></returns>
        public bool Unlucky1(int[] numbers)
        {

            int larray = numbers.Length;
            if ((numbers[0] == 1) && (numbers[1] == 3) || (numbers.Last() == 3) && (numbers[larray - 2] == 1) || (numbers[1] == 1) && (numbers[2] == 3) || (numbers[larray - 2] == 3) && (numbers[larray - 3] == 1))
            {
                return true;
            }
            return false;

        }
        ///UGH
        /// <summary>
        /// Ex.#15
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public int[] make2(int[] a, int[] b)
        {
            int[] tempArry = new int[2];
            int count = 0;

            if (a.Length >= 2)
            {
                tempArry[0] = a[0];
                tempArry[1] = a[1];
                return tempArry;
            }
            else if (a.Length == 1)
            {
                tempArry[0] = a[0];
                tempArry[1] = b[0];
                return tempArry;
            }
            else
            {
                tempArry[0] = b[0];
                tempArry[1] = b[1];
                return tempArry;
            }

        }
    }
}
