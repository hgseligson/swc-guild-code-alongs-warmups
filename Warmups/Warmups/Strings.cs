﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warmups
{

    public class Strings
    {
        /// <summary>
        /// Ex. #1
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public string SayHi(string name)
        {
            string hello = "Hello ";
            string point = "!";

            return hello + name + point;
        }
        /// <summary>
        /// Ex.#2
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public string Abba(string a, string b)
        {
            return a + b + b + a;
        }

        /// <summary>
        /// Ex. #3
        /// </summary>
        /// <param name="tag"></param>
        /// <param name="content"></param>
        /// <returns></returns>
        public string MakeTags(string tag, string content)
        {
            return "<" + tag + ">" + content + "</" + tag + ">";
        }
        /// <summary>
        /// Ex,#4
        /// </summary>
        /// <param name="container"></param>
        /// <param name="word"></param>
        /// <returns></returns>
        public string InsertWord(string container, string word)
        {
            string s = container.Substring(0, 2);
            string r = container.Substring(2);

            return s + word + r;
        }
        /// <summary>
        /// Ex. #5
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public string MultipleEndings(string str)
        {
            return str.Substring(str.Length - 2) + str.Substring(str.Length - 2) + str.Substring(str.Length - 2);
        }
        /// <summary>
        /// Ex.#6
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public string FirstHalf(string str)
        {
            int x = str.Length / 2;
            string result = str.Substring(0, x);
            return result;
        }
        /// <summary>
        /// Ex. #7
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public string TrimOne(string str)
        {
            return str.Substring(1, str.Length - 2);
        }
        /// <summary>
        /// Ex.#8
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public string LongInMiddle(string a, string b)
        {
            int aL = a.Length;
            int bL = b.Length;
            if (aL > bL)
            {
                return b + a + b;
            }
            return a + b + a;
        }
        /// <summary>
        /// Ex. #9
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public string RotateLeft2(string str)
        {
            return str.Substring(2) + str.Substring(0, 2);
        }
        /// <summary>
        /// Ex.#10
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public string ex10(string str)
        {
            int strLength = str.Length;
            string last2 = str.Substring(strLength - 2);
            string restStr = str.Substring(0, strLength - 2);
            return last2 + restStr;
        }
        /// <summary>
        /// Ex.#11
        /// </summary>
        /// <param name="str"></param>
        /// <param name="fromFront"></param>
        /// <returns></returns>
        public string TakeOne(string str, bool fromFront)
        {
            if (fromFront == false)
            {
                return str[str.Length - 1].ToString();
            }
            return str[0].ToString();
        }
        /// <summary>
        /// Ex.#12
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public string MiddleTwo(string str)
        {
            int halfStrLength = str.Length / 2;
            string newStr = str.Substring(halfStrLength - 1, 2);
            return newStr;
        }

        /// <summary>
        /// Ex. #13
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public bool EndsWithLy(string str)
        {
            if (str.Contains("l"))
            {
                if (str[str.Length - 1] == 'y')
                {
                    return true;
                }
            }
            return false;
        }
        /// <summary>
        /// Ex.#14
        /// </summary>
        /// <param name="str"></param>
        /// <param name="n"></param>
        /// <returns></returns>
        public string FrontAndBack(string str, int n)
        {
            int strLength = str.Length;
            string fStr = str.Substring(0, n);
            string lStr = str.Substring(strLength - n);
            return fStr + lStr;
        }
        /// <summary>
        /// Ex.#15
        /// </summary>
        /// <param name="str"></param>
        /// <param name="n"></param>
        /// <returns></returns>
        public string TakeTwoFromPosition(string str, int n)
        {
            if (n <= str.Length - 2)
            {
                str = str.Substring(n, 2);
            }
            else if (n > str.Length - 2)
            {
                str = str.Substring(0, 2);
            }
            return str;
        }
        /// <summary>
        /// Ex.#16
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public bool HasBad(string str)
        {
            if (str.StartsWith("bad"))
            {
                return true;
            }
            else if (str.Substring(1, 3) == "bad")
            {
                return true;
            }
            return false;
        }
        /// <summary>
        /// Ex.#17
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public string AtFirst(string str)
        {
            if (str.Length < 2)
            {
                str = str + '@';
                return str;
            }

            str = str[0].ToString() + str[1];
            return str;
        }
        /// <summary>
        /// Ex.#18
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public string LastChars(string a, string b)
        {

            if (a == "")
            {
                a = "@";
            }
            else if (b == "")
            {
                b = "@";
            }
            string aFirst = a.Substring(0, 1);

            int bLength = b.Length;
            string bLast = b.Substring(bLength - 1, 1);
            return aFirst + bLast;
        }

        /// <summary>
        /// Ex.#19
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public string ConCat(string a, string b)
            {
            if ((b.Length != 0) && (a.Length != 0))
            {
                if (a[a.Length - 1] == b[0])
                {
                    b = b.Substring(1);
                    a = a + b;
                    return a;
                }
            }
            a = a + b;
            return a;
            }
        /// <summary>
        /// Ex.#20
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public string SwapLast(string str)
        {
            int length = str.Length;

            string last = str.Substring(length - 1);
            string TwoLast = str.Substring(length - 2, 1);
            string rest = str.Substring(0, length - 2);
            return rest + last + TwoLast;
        }
        /// <summary>
        /// Ex.#21
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public bool FrontAgain(string str)
        {
            string front = str.Substring(0, 2);
            string back = str[str.Length - 2].ToString() + str[str.Length - 1];
            if (front == back)
            {
                return true;
            }

            return false;
        }
        /// <summary>
        /// Ex.#22
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public string MinCat(string a, string b)
        {
            int aLength = a.Length;
            int bLength = b.Length;

            if (aLength > bLength)
            {
                string newA = a.Substring(aLength - bLength);
                return newA + b;
            }
            else if (aLength < bLength)
            {
                string newB = b.Substring(bLength - aLength);
                return a + newB;
            }

            return a + b;
        }

        /// <summary>
        /// Ex.#23 -Given a string, return a version without the first 2 chars. 
        /// Except keep the first char if it is 'a' and keep the 
        /// second char if it is 'b'. The string may be any length.
        /// </summary>
        /// <param name = "str" ></ param >
        /// < returns ></ returns >
        public string TweakFront(string str)
        {
            string result = "";
            for (int i = 0; i < str.Length; i++)
            {
                if (str[i] == 'a' && i == 0)
                {
                    result += str[i];
                }else if (str[i] == 'b' && i == 1)
                {
                    result += str[i];

                }
                else if(i > 1)
                {
                    result += str[i];

                }
            }
            return result;
        }
        /// <summary>
        /// Ex.#24
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public string StripX(string str)
        {
            string result = str;
            if (result.StartsWith("x"))
            {
                result = result.Substring(1);


            }
            if (result.EndsWith("x"))
            {
                result = result.Substring(0, result.Length - 1);
            }
            return result;


        }
    }
}


