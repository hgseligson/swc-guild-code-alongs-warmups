﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework.Constraints;
using NUnit.Framework;

namespace Warmups.Tests
{
    
    public class ConditionalsTests
    {
        //ex.#1
        [TestCase(true, true, true)]
        [TestCase(true, false, false)]
        [TestCase(false, false, true)]
        public void AreWeInTrouble_Tests(bool aSmile, bool bSmile, bool expectedResult)
        {
            Conditionals cond = new Conditionals();

            Assert.AreEqual(expectedResult, cond.AreWeInTrouble(aSmile, bSmile));
        }
        //ex.#2
        [TestCase(false, false, true)]
        [TestCase(false, true, true)]
        public void CanSleepIn_Tests(bool vaca, bool weekday, bool expectedResult)
        {
            Conditionals cond = new Conditionals();

            Assert.AreEqual(expectedResult, cond.CanSleepIn(vaca, weekday));
        }
        //ex.#3
        [TestCase(1, 2, 3)]
        [TestCase(3, 2 , 5)]
        public void SumDouble_Tests(int a, int b, int expectedResult)
        {
            Conditionals cond = new Conditionals();
            int actual = cond.SumDouble(a, b);
            Assert.AreEqual(expectedResult, actual);
        }
        //ex.#4
        [TestCase(23, 4)]
        [TestCase(10, 11)]
        public void Diff21_Tests(int n, int expectedResult)
        {
            Conditionals cond = new Conditionals();

            Assert.AreEqual(expectedResult, cond.Diff21(n));
        }
        //ex.#5
        [TestCase(true, 6, true)]
        [TestCase(true, 7, false)]
        public void ParrotTrouble_Test(bool isTalking, int hour, bool expectedResult)
        {
            Conditionals cond = new Conditionals();

            Assert.AreEqual(expectedResult, cond.ParrotTrouble(isTalking, hour));
        }
        //ex.#6
        [TestCase(9, 10, true)]
        [TestCase(9, 9, false)]
        public void Makes10(int a, int b, bool expectedResult)
        {
            Conditionals cond = new Conditionals();

            Assert.AreEqual(expectedResult, cond.Makes10(a,b));
        }
        //ex.#7
        [TestCase(103, true)]
        [TestCase(90, true)]
        [TestCase(89, false)]
        public void NearHundred_Tests(int n, bool expectedResult)
        {
            Conditionals cond = new Conditionals();

            Assert.AreEqual(expectedResult, cond.NearHundred(n));
        }
        //ex.#8
        [TestCase(1, -1, false, true)]
        [TestCase(-1, 1, false, true)]
        [TestCase(-4, -5, true, true)]
        public void PosNeg_Tests(int a, int b, bool negative, bool ExpectedResult)
        {
            Conditionals cond = new Conditionals();

            Assert.AreEqual(ExpectedResult, cond.PosNeg(a, b, negative));
        }
        //ex.#9
        [TestCase("candy", "not candy")]
        [TestCase("x", "not x")]
        [TestCase("not bad", "not bad")]
        public void NotString_Tests(string s, string expectedResult)
        {
            Conditionals cond = new Conditionals();

            Assert.AreEqual(expectedResult, cond.NotString(s));
        }
        //ex.#10
        [TestCase("kitten", 1, "ktten")]
        [TestCase("kitten", 0, "itten")]
        [TestCase("kitten", 4, "kittn")]
        public void MissingChar_Test(string str, int n, string ExpectedResult)
        {
            Conditionals cond = new Conditionals();

            Assert.AreEqual(ExpectedResult, cond.MissingChar(str, n));
        }
        //ex.#11
        [TestCase("code", "eodc")]
        [TestCase("ab", "ba")]
        public void FrontBack_Test(string str, string ExpectedResult)
        {
            Conditionals cond = new Conditionals();

            Assert.AreEqual(ExpectedResult, cond.FrontBack(str) );
        }
        //ex.#12
        [TestCase("Microsoft", "MicMicMic")]
        [TestCase("Chocolate", "ChoChoCho")]
        [TestCase("at", "atatat")]
        public void Front3_Test(string str, string ExpectedValue)
        {
            Conditionals cond = new Conditionals();

            Assert.AreEqual(ExpectedValue, cond.Front3(str));
        }
        //ex.#13
        [TestCase("cat", "tcatt")]
        [TestCase("Hello", "oHelloo")]
        public void BackAround_Test(string str, string ExpectedValue)
        {
            Conditionals cond = new Conditionals();

            Assert.AreEqual(ExpectedValue, cond.BackAround(str) );
        }
        //ex.#14
        [TestCase(3, true)]
        [TestCase(8, false)]
        public void Mutiple3or5_Test(int number, bool ExpectedResult)
        {
            Conditionals cond = new Conditionals();

            Assert.AreEqual(ExpectedResult, cond.Multiple3or5(number));
        }
        //ex.#15
        [TestCase("hi there", true)]
        [TestCase("high up", false)]
        public void StartHi_Test(string str, bool ExpectedResult)
        {
            Conditionals cond = new Conditionals();

            Assert.AreEqual(ExpectedResult, cond.StartHi(str));
        }
        //ex.#16
        [TestCase(120, -1, true)]
        [TestCase(-1, 120, true)]
        public void IcyHot_Tests(int temp1, int temp2, bool expectedResult)
        {
            Conditionals cond = new Conditionals();

            Assert.AreEqual(expectedResult, cond.IcyHot(temp1, temp2));
        }
        //ex.#17
        [TestCase(12, 99, true)]
        [TestCase(8, 99, false)]
        public void Between10and20_Test(int a, int b, bool ExpectedResult)
        {
            Conditionals cond = new Conditionals();

            Assert.AreEqual(ExpectedResult, cond.Between10and20(a,b));
        }
        //ex.#18
        [TestCase(13, 20, 10, true)]
        [TestCase(20, 19, 10, true)]
        public void HasTeen_Tests(int a, int b, int c, bool expectedResult)
        {
            Conditionals cond = new Conditionals();

            Assert.AreEqual(expectedResult, cond.HasTeen(a, b, c));
        }
        //ex.#19
        [TestCase(13, 99, true)]
        [TestCase(13, 13, false)]
        public void SoAlone_Test(int a, int b, bool ExpectedResult)
        {
            Conditionals cond = new Conditionals();

            Assert.AreEqual(ExpectedResult, cond.SoAlone(a,b));
        }
        //ex.#20
        [TestCase("adelbc", "abc")]
        [TestCase("adelHello", "aHello")]
        public void RemoveDel_Test(string str, string expectedResult)
        {
            Conditionals cond = new Conditionals();

            Assert.AreEqual(expectedResult, cond.RemoveDel(str));
        }
        //ex.#21
        [TestCase("mix snacks", true)]
        [TestCase("piz snacks", false)]
        public void IxStart_Test(string str, bool ExpectedValue)
        {
            Conditionals cond = new Conditionals();

            Assert.AreEqual(ExpectedValue, cond.IxStart(str));
        }
        //ex.#22
        [TestCase("ozymandias", "oz")]
        [TestCase("bzoo", "z")]
        public void StartOz_Tests(string str, string expectedResult)
        {
            Conditionals cond = new Conditionals();

            Assert.AreEqual(expectedResult, cond.StartOz(str));
        }
        //ex.#23
        [TestCase(1, 2, 3,3)]
        [TestCase(1,3,2,3)]
        public void Max_Test(int a, int b, int c, int ExpectedResult)
        {
            Conditionals cond = new Conditionals();

            Assert.AreEqual(ExpectedResult, cond.Max(a,b,c));
        }
        //ex.#24
        [TestCase(8, 13, 8)]
        [TestCase(13, 8, 8)]
        public void Closer_Test(int a, int b, int expectedResult)
        {
            Conditionals cond = new Conditionals();

            Assert.AreEqual(expectedResult, cond.Closer(a, b));
        }
        //ex.#25
        [TestCase("Hello", true)]
        [TestCase("Heelle", true)]
        public void GotE_Test(string str, bool ExpectedValue)
        {
            Conditionals cond = new Conditionals();

            Assert.AreEqual(ExpectedValue, cond.GotE(str));
        }
        //ex.#26
        [TestCase("Hello", "HeLLO")]
        [TestCase("hi there", "hi thERE")]
        public void EndUp_Test(string str, string expectedResult)
        {
            Conditionals cond = new Conditionals();

            Assert.AreEqual(expectedResult, cond.EndUp(str));
        }
        //ex.#27
        [TestCase("Miracle", 2, "Mrce")]
        [TestCase("abcdefg", 2, "aceg")]
        public void EveryNth_Test(string str, int n, string ExpectedResult)
        {
            Conditionals cond = new Conditionals();

            Assert.AreEqual(ExpectedResult, cond.EveryNth(str, n));
        }
    }
}

