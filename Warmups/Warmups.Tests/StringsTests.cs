﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Warmups.Tests
{
    class StringsTests
    {
        //ex.#1
        [TestCase("Bob", "Hello Bob!")]
        [TestCase("Alice", "Hello Alice!")]

        public void SayHi_Test(string name, string ExpectedResult)
        {
            Strings test = new Strings();
            Assert.AreEqual(ExpectedResult, test.SayHi(name));
        }
        //ex.#2
        [TestCase("Hi", "Bye", "HiByeByeHi")]
        [TestCase("Yo", "Alice", "YoAliceAliceYo")]
        [TestCase("What", "Up", "WhatUpUpWhat")]
        public void Abba_Test(string a, string b, string expectedResult)
        {
            Strings str = new Strings();
            Assert.AreEqual(expectedResult, str.Abba(a, b));
        }

        //ex.#3
        [TestCase("i", "Yay", "<i>Yay</i>")]
        [TestCase("i", "Hello", "<i>Hello</i>")]

        public void MakeTags_Test(string tag, string content, string ExpectedResult)
        {
            Strings test = new Strings();
            Assert.AreEqual(ExpectedResult, test.MakeTags(tag, content));
        }

        //ex.#4
        [TestCase("<<>>", "Yay", "<<Yay>>")]
        [TestCase("<<>>", "WooHoo", "<<WooHoo>>")]
        [TestCase("[[]]", "word", "[[word]]")]
        public void InsertWord(string container, string word, string expectedResult)
        {
            Strings str = new Strings();
            Assert.AreEqual(expectedResult, str.InsertWord(container, word));
        }
    
        //ex.#5
        [TestCase("Hello", "lololo")]
        [TestCase("ab", "ababab")]

        public void MultipleEndings_Test(string str, string ExpectedResult)
        {
            Strings test = new Strings();
            Assert.AreEqual(ExpectedResult, test.MultipleEndings(str));
        }
        //ex.#6
        [TestCase("WooHoo", "Woo")]
        [TestCase("HelloThere", "Hello")]
        [TestCase("abcdef", "abc")]
        public void FirstHalf_Test(string str, string expectedResult)
        {
            Strings stri = new Strings();
            Assert.AreEqual(expectedResult, stri.FirstHalf(str));
        }
        //ex.#7
        [TestCase("Hello", "ell")]
        [TestCase("java", "av")]

        public void TrimOne_Test(string str, string ExpectedResult)
        {
            Strings test = new Strings();
            Assert.AreEqual(ExpectedResult, test.TrimOne(str));
        }
        //ex.#8
        [TestCase("Hello", "hi", "hiHellohi")]
        [TestCase("hi", "Hello", "hiHellohi")]
        [TestCase("aaa", "b", "baaab")]
        public void LongInMiddle_Test(string a, string b, string expectedResult)
        {
            Strings str = new Strings();
            Assert.AreEqual(expectedResult, str.LongInMiddle(a, b));
        }

        //ex.#9
        [TestCase("Hello", "lloHe")]
        [TestCase("java", "vaja")]

        public void RotateLeft2_Test(string str, string ExpectedResult)
        {
            Strings test = new Strings();
            Assert.AreEqual(ExpectedResult, test.RotateLeft2(str));
        }

        //ex.#10
        [TestCase("Hello", "loHel")]
        [TestCase("java", "vaja")]
        [TestCase("Hi", "Hi")]
        public void ex10_Test(string str, string expectedResult)
        {
            Strings stri = new Strings();
            Assert.AreEqual(expectedResult, stri.ex10(str));
        }

        //ex.#11
        [TestCase("Hello", true, "H")]
        [TestCase("Hello", false, "o")]

        public void TakeOne_Test(string str, bool fromFront, string ExpectedResult)
        {
            Strings test = new Strings();
            Assert.AreEqual(ExpectedResult, test.TakeOne(str, fromFront));
        }
        //ex.#12
        [TestCase("string", "ri")]
        [TestCase("code", "od")]
        [TestCase("Practice", "ct")]
        public void MiddleTwo_Test(string str, string expectedResult)
        {
            Strings stri = new Strings();
            Assert.AreEqual(expectedResult, stri.MiddleTwo(str));
        }

        //ex.#13
        [TestCase("oddly", true)]
        [TestCase("oddt", false)]

        public void EndsWithLy_Test(string str, bool ExpectedResult)
        {
            Strings test = new Strings();
            Assert.AreEqual(ExpectedResult, test.EndsWithLy(str));
        }
        //ex.#14
        [TestCase("Hello", 2, "Helo")]
        [TestCase("Chocolate", 3, "Choate")]
        [TestCase("Chocolate", 1, "Ce")]
        public void FrontAndBack_Test(string str, int n, string expectedResult)
        {
            Strings stri = new Strings();
            Assert.AreEqual(expectedResult, stri.FrontAndBack(str, n));
        }
        //ex.#15
        [TestCase("java", 0, "ja")]
        [TestCase("java", 2, "va")]

        public void TakeTwoFromPosition_Test(string str, int n, string ExpectedResult)
        {
            Strings test = new Strings();
            Assert.AreEqual(ExpectedResult, test.TakeTwoFromPosition(str, n));
        }
        //ex.#16
        [TestCase("badxx", true)]
        [TestCase("xbadxx", true)]
        [TestCase("xxbadxx", false)]
        public void HasBad_Test(string str, bool expectedResult)
        {
            Strings stri = new Strings();
            Assert.AreEqual(expectedResult, stri.HasBad(str));
        }
        //ex.#17
        [TestCase("hello", "he")]
        [TestCase("hi", "hi")]

        public void AtFirst_Test(string str, string ExpectedResult)
        {
            Strings test = new Strings();
            Assert.AreEqual(ExpectedResult, test.AtFirst(str));
        }
        //ex.#18
        [TestCase("last", "chars", "ls")]
        [TestCase("yo", "mama", "ya")]
        [TestCase("hi", "", "h@")]
        public void LastChars_Test(string a, string b, string expectedResult)
        {
            Strings str = new Strings();
            Assert.AreEqual(expectedResult, str.LastChars(a, b));

        }
        //ex.#19
        [TestCase("abc", "cat", "abcat")]
        [TestCase("dog", "cat")]

        public void ConCat_Test(string a, string b, string ExpectedResult)
        {
            Strings test = new Strings();
            Assert.AreEqual(ExpectedResult, test.ConCat(a, b));
        }

        //ex.#20
        [TestCase("coding", "codign")]
        [TestCase("cat", "cta")]
        [TestCase("ab", "ba")]
        public void SwapLast_Test(string str, string expectedResult)
        {
            Strings stri = new Strings();
            Assert.AreEqual(expectedResult, stri.SwapLast(str));
        }

        //ex.#21
        [TestCase("edited", true)]
        [TestCase("edit", false)]

        public void FrontAgain_Test(string str, bool ExpectedResult)
        {
            Strings test = new Strings();

            Assert.AreEqual(ExpectedResult, test.FrontAgain(str));
        }
        //ex.#22
        [TestCase("Hello", "Hi", "loHi")]
        [TestCase("Hello", "java", "ellojava")]
        [TestCase("java", "Hello", "javaello")]
        public void MinCat_Test(string a, string b, string expectedResult)
        {
            Strings str = new Strings();
            Assert.AreEqual(expectedResult, str.MinCat(a, b));
        }
        //ex.#23
        [TestCase("Hello", "llo")]
        [TestCase("away", "aay")]
        [TestCase("abed", "abed")]
    
        public void TweakFront_Test(string str, string ExpectedResult)
        {
            Strings test = new Strings();

            Assert.AreEqual(ExpectedResult, test.TweakFront(str));

        }
        //ex.#24
        [TestCase("xHix", "Hi")]
        [TestCase("xHi", "Hi")]
        [TestCase("Hxix", "Hxi")]
        public void StripX_Test(string str, string expectedResult)
        {
            Strings stri = new Strings();
            Assert.AreEqual(expectedResult, stri.StripX(str));
        }
    }
}
