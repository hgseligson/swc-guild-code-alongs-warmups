﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;


namespace Warmups.Tests
{
    public class LoopsTest
    {
        //ex.1
        [TestCase("Hi", 2, "HiHi")]
        [TestCase("Hi", 3, "HiHiHi")]
        
        public void StringTimes_Test(string str, int n, string expectedResult)
        {
            Loops test = new Loops();
            Assert.AreEqual(expectedResult, test.StringTimes(str,n));
        }
        //ex.2
        [TestCase("Chocolate", 2, "ChoCho")]
        [TestCase("Chocolate", 3, "ChoChoCho")]
        
        public void FrontTime_Test(string str, int n, string expectedResult)
        {
            Loops test = new Loops();
            Assert.AreEqual(expectedResult, test.FrontTimes(str, n));
        }
        //ex.3
        [TestCase("abcxx", 1)]
        [TestCase("xxx", 2)]
        
        public void CountXX_Test(string str, int expectedResult)
        {
            Loops test = new Loops();
            Assert.AreEqual(expectedResult, test.CountXX(str));
        }
        //ex.#4
        [TestCase("axxbb", true)]
        [TestCase("axaxxax", false)]
        [TestCase("axaxxax", false)]
        public void DoubleX_Test(string str, bool expectedResult)
        {
            Loops loo = new Loops();
            Assert.AreEqual(expectedResult, loo.DoubleX(str));
        }
        //ex.5
        [TestCase("Hello", "Hlo")]
        [TestCase("Hi", "H")]
        public void EveryOther(string str, string expectedResult)
        {
            Loops test = new Loops();
            Assert.AreEqual(expectedResult, test.EveryOther(str));
        }
        //ex.#6
        [TestCase("Code", "CCoCodCode")]
        [TestCase("abc", "aababc")]
        [TestCase("ab", "aab")]
        public void StringSplosion_Test(string str, string expectedResult)
        {
            Loops loo = new Loops();
            Assert.AreEqual(expectedResult, loo.StringSplosion(str));
        }

        //ex.7
        [TestCase("hixxhi", 1)]
        [TestCase("xaxxaxaxx", 1)]
        public void CountLast2(string str, string expectedResult)
        {
            Loops test = new Loops();
            Assert.AreEqual(expectedResult, test.CountLast2(str));
        }
        //ex.#8
        [TestCase(new int[] { 1, 2, 9 }, 1)]
        [TestCase(new int[] { 1, 9, 9 }, 2)]
        [TestCase(new int[] { 1, 9, 9, 3, 9 }, 3)]
        public void count9_Test(int[] numbers, int expectedResult)
        {
            Loops loo = new Loops();
            Assert.AreEqual(expectedResult, loo.Count9(numbers));
        }
        //ex.9
        [TestCase(new int[] { 1, 2, 9, 3, 4 }, true)]
        [TestCase(new int[] { 1, 2, 3, 4, 9 }, false)]
        public void ArrayFront9_Test(int[] numbers, bool expectedResult)
        {
            Loops test = new Loops();

            Assert.AreEqual(expectedResult,test.ArrayFront9(numbers));
        }
        //ex.#10
        [TestCase(new int[] { 1, 1, 2, 3, 1 }, true)]
        [TestCase(new int[] { 1, 1, 2, 4, 1 }, false)]
        [TestCase(new int[] { 1, 1, 2, 1, 2, 3 }, true)]
        public void Array123_Test(int[] numbers, bool expectedResult)
        {
            Loops loo = new Loops();
            Assert.AreEqual(expectedResult, loo.Array123(numbers));
        }

        //Ex.#11
        [TestCase(new int[] {1, 2, 9, 3, 4}, true)]
        [TestCase(new int[] {1, 2, 3, 4, 9}, false)]       
        public void SubStringMatch_Test(string a, string b, string expectedResult)
        {
            Loops test = new Loops();

            Assert.AreEqual(expectedResult, test.SubStringMatch(a,b));
        }

        //ex.#12
        [TestCase("xxHxix", "xHix")]
        [TestCase("abxxxcd", "abcd")]
        [TestCase("xabxxxcdx", "xabcdx")]
        public void StringX_Test(string str, string expectedResult)
        {
            Loops test = new Loops();
            Assert.AreEqual(expectedResult, test.StringX(str));
        }

        //ex.#13
        [TestCase("kitten", "kien")]
        [TestCase("Chocolate", "Chole")]
        public void AltPairs_Test(string str, string expectedResult)
        {
            Loops loo = new Loops();
            Assert.AreEqual(expectedResult, loo.AltPairs(str));
        }
        //ex.#14
        [TestCase("yakpak", "pak")]
        [TestCase("pakyak", "pak")]
        [TestCase("yak123ya", "123ya")]
        public void DonNotYak_Test(string str, string expectedResult)
        {
            Loops loo = new Loops();
            Assert.AreEqual(expectedResult, loo.DoNotYak(str));
        }

        //ex.#15
        [TestCase(new int[] {6, 6, 2}, 1)]
        [TestCase(new int[] {6, 6, 2, 6}, 1)]
        public void Array667_Test(int[] numbers, int expectedResult)
        {
            Loops loo = new Loops();
            Assert.AreEqual(expectedResult, loo.Array667(numbers));
        }
    }  
 }
   

