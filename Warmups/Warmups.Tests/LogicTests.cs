﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Warmups.Tests
{
    public class LogicTests
    {
        //Ex.#1
        [TestCase(30, false, false)]
        [TestCase(50, false, true)]
        public void GreatParty_Test(int cigars, bool isWeekend, bool expectedResult)
        {
            Logic target = new Logic();
            bool actual = target.GreatParty(cigars, isWeekend);
            Assert.AreEqual(expectedResult, actual);
        }
        //Ex.#2
        [TestCase(5, 2, 0)]
        [TestCase(5, 5, 1)]
        public void CanHazTable_Test(int yourStyle, int dateStyle, int expectedResult)
        {
            Logic log = new Logic();
            Assert.AreEqual(expectedResult, log.CanHazTable(yourStyle, dateStyle));
        }

        //Ex.#3
        [TestCase(70, false, true)]
        [TestCase(95, false, false)]

        public void PlayOutside_Test(int temp, bool isSummer, bool expectedResult)
        {
            Logic target = new Logic();
            bool actual = target.PlayOutside(temp, isSummer);
            Assert.AreEqual(expectedResult, actual);
        }
        //Ex.#4
        [TestCase(60, false, 0)]
        [TestCase(65, false, 1)]
        [TestCase(65, true, 0)]
        public void CaughtSpeeding_test(int speed, bool isBirthday, int expectedResult)
        {
            Logic log = new Logic();
            Assert.AreEqual(expectedResult, log.CaughtSpeeding(speed, isBirthday));
        }

        //Ex.#5
        [TestCase(70, false, true)]
        [TestCase(95, false, false)]

        public void SkipSum_Test(int a, int b, bool SkipSum, bool expectedResult)
        {
            Logic log = new Logic();

            Assert.AreEqual(expectedResult, log.SkipSum(a,b));
        }
        //Ex.#6
        [TestCase(1, false, "7:00")]
        [TestCase(5, false, "7:00")]
        [TestCase(0, false, "10:00")]
        public void AlarmClock_Test(int day, bool vacation, string expectedResult)
        {
            Logic log = new Logic();
            Assert.AreEqual(expectedResult, log.AlarmClock(day, vacation));
        }
        //Ex.#7
        [TestCase(6, 4, true)]
        [TestCase(4, 5, false)]
        public void LoveSix_Test(int a, int b, bool expectedResult)
        {
            Logic log = new Logic();
            Assert.AreEqual(expectedResult, log.LoveSix(a,b));
        }
        //Ex.#8
        [TestCase(5, false, true)]
        [TestCase(11, false, false)]
        [TestCase(11, true, true)]
        public void InRange_Test(int n, bool outsideMode, bool expectedResult)
        {
            Logic log = new Logic();
            Assert.AreEqual(expectedResult, log.InRange(n, outsideMode));
        }
        //Ex.#9
        [TestCase(22, true)]
        [TestCase(23, true)]
        public void SpecialEleven_Test(int n, bool expectedResult)
        {
            Logic log = new Logic();
            Assert.AreEqual(expectedResult, log.SpecialEleven(n));
        }
        //Ex.#10
        [TestCase(20, false)]
        [TestCase(21, true)]
        [TestCase(22, true)]
        public void Mod20_Test(int n, bool expectedResult)
        {
            Logic log = new Logic();
            Assert.AreEqual(expectedResult, log.Mod20(n));
        }
        //Ex.#11
        [TestCase(3, true)]
        [TestCase(10, true)]
        public void Mod35_Test(int n, bool expectedResult)
        {
            Logic log = new Logic();
            Assert.AreEqual(expectedResult, log.Mod35(n));
        }
        //Ex.#12
        [TestCase(false, false, false, true)]
        [TestCase(false, false, true, false)]
        [TestCase(true, false, false, false)]
        public void AnswerCell_Test(bool isMorning, bool isMom, bool isAsleep, bool expectedResult)
        {
            Logic log = new Logic();
            Assert.AreEqual(expectedResult, log.AnswerCell(isMorning, isMom, isAsleep));
        }
        //Ex.#13
        [TestCase(1, 2, 3, true)]
        [TestCase(3, 1, 2, true)]
        public void TwoIsOne_Test(int a, int b, int c, bool expectedResult)
        {
            Logic log = new Logic();
            Assert.AreEqual(expectedResult, log.TwoIsOne(a,b,c));
        }
        //Ex.#14
        [TestCase(1, 2, 4, false, true)]
        [TestCase(1, 2, 1, false, false)]
        [TestCase(1, 1, 2, true, true)]
        public void AreInOrder(int a, int b, int c, bool bOk, bool expectedResult)
        {
            Logic log = new Logic();
            Assert.AreEqual(expectedResult, log.AreInOrder(a, b, c, bOk));
        }

        //Ex.#15
        [TestCase(2, 5, 11, false)]
        [TestCase(5, 7, 6, false)]
        public void InOrderEqual_Test(int a, int b, int c, bool equalOk, bool expectedResult)
        {
            Logic log = new Logic();
            Assert.AreEqual(expectedResult, log.InOrderEqual(a, b, c, equalOk));
        }
        //Ex.#16
        [TestCase(23, 19, 13, true)]
        [TestCase(23, 19, 12, false)]
        [TestCase(23, 19, 3, true)]
        public void LastDigit_Test(int a, int b, int c, bool expectedResult)
        {
            Logic log = new Logic();
            Assert.AreEqual(expectedResult, log.LastDigit(a, b, c));
        }
        //Ex.#17
        [TestCase(2, 3, true, 5)]
        [TestCase(3, 3, true, 7)]
        [TestCase(3, 3, false, 6)]
        public void RollDice_Test(int die1, int die2, bool noDoubles, int expectedResult)
        {
            Logic log = new Logic();
            int actual = log.RollDice(die1, die2, noDoubles);
            Assert.AreEqual(expectedResult, actual);
        }
    }
}
