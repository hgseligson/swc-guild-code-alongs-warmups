﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Warmups.Tests
{
    [TestFixture]
    public class ArrayTests
    {
        //Ex.#1
        [TestCase(new int[] {1, 2, 6}, true)]
        [TestCase(new int[] {6, 1, 2, 3}, true)]

        public void FirstLast6_Test(int[] numbers, bool expectedResult)
        {
            Arrays test = new Arrays();

            Assert.AreEqual(expectedResult, test.FirstLast6(numbers));
        }
        //Ex.#2
        [TestCase(new int[] {1, 2, 3}, false)]
        [TestCase(new int[] {1, 2, 3, 1}, true)]
        [TestCase(new int[] {1, 2, 1}, true)]
        public void SameFirstLast_Test(int[] numbers, bool expectedResult)
        {
            Arrays array = new Arrays();
            Assert.AreEqual(expectedResult, array.SameFirstLast(numbers));
        }

        //Ex.#3
        [TestCase(3, new int[] { 3, 1, 4 })]
        public void MakePi_Test(int n, int expectedResult)
        {
            Arrays arr = new Arrays();
            Assert.AreEqual(expectedResult, arr.MakePi(n));
        }

        //Ex.#4
        [TestCase(new int[] { 1, 2, 3 }, new int[] { 7, 3 }, true)]
        [TestCase(new int[] { 1, 2, 3 }, new int[] { 7, 3, 2 }, false)]
        [TestCase(new int[] { 1, 2, 3 }, new int[] { 1, 3 }, true)]
        public void commonend_Test(int[] a, int[] b, bool expectedResult)
        {
            Arrays arr = new Arrays();
            Assert.AreEqual(expectedResult, arr.commonEnd(a, b));
        }

        //Ex.#5
        [TestCase(new int[] {1, 2, 3}, 6)]
        [TestCase(new int[] {5, 11, 2}, 18)]
        public void Sum_Test(int[] numbers, int expectedResult)
        {
            Arrays arr = new Arrays();
            Assert.AreEqual(expectedResult, arr.Sum(numbers));
        }

        //Ex.#6
        [TestCase(new int[] { 1, 2, 3 }, new int[] { 2, 3, 1 })]
        [TestCase(new int[] { 5, 11, 9 }, new int[] { 11, 9, 5 })]
        [TestCase(new int[] { 7, 0, 0 }, new int[] { 0, 0, 7 })]
        public void RotateLeft_test(int[] numbers, int[] expectedResult)
        {
            Arrays arr = new Arrays();
            Assert.AreEqual(expectedResult, arr.RotateLeft(numbers));
        }

        //Ex.#7
        [TestCase(new int[] {1, 2, 3}, new int[] {3, 2, 1})]
        public void Reverse_test(int[] numbers, int[] expectedResult)
        {
            Arrays arr = new Arrays();
            Assert.AreEqual(expectedResult, arr.Reverse(numbers));
        }

        //Ex.#8
        [TestCase(new int[] { 1, 2, 3 }, new int[] { 3, 3, 3 })]
        [TestCase(new int[] { 11, 5, 9 }, new int[] { 11, 11, 11 })]
        [TestCase(new int[] { 2, 11, 3 }, new int[] { 3, 3, 3 })]
        public void HigherWins_Test(int[] numbers, int[] expectedResult)
        {
            Arrays arr = new Arrays();
            Assert.AreEqual(expectedResult, arr.HigherWins(numbers));
        }
        //Ex.#9
        [TestCase(new int[] {1, 2, 3}, new int[] {4, 5, 6}, new int[] {2, 5})]
        [TestCase(new int[] {7, 7, 7}, new int[] {3, 8, 0}, new int[] {7, 8})]
        [TestCase(new int[] {5, 2, 9}, new int[] {1, 4, 5}, new int[] {2, 4})]
        public void GetMiddle_Test(int[] a, int[] b, int[] expectedResult)
        {
            Arrays arr = new Arrays();
            Assert.AreEqual(expectedResult, arr.GetMiddle(a,b));
        }
        //Ex.#10
        [TestCase(new int[] { 2, 5 }, true)]
        [TestCase(new int[] { 4, 3 }, true)]
        [TestCase(new int[] { 7, 5 }, false)]
        public void HasEven_Test(int[] numbers, bool expectedResult)
        {
            Arrays arr = new Arrays();
            Assert.AreEqual(expectedResult, arr.HasEven(numbers));
        }
        //Ex.#11
        [TestCase(new int[] {4, 5, 6}, new int[] {0, 0, 0, 0, 0, 6})]
        [TestCase(new int[] {1, 2}, new int[] {0, 0, 0, 2})]
        [TestCase(new int[] {3}, new int[] {0, 3})]
        public void KeepLast_Test(int[] numbers, int[] expectedResult)
        {
            Arrays arr = new Arrays();
            Assert.AreEqual(expectedResult, arr.KeepLast(numbers));
        }
        //Ex.#12
        [TestCase(new int[] { 2, 2, 3 }, true)]
        [TestCase(new int[] { 3, 4, 5, 3 }, true)]
        [TestCase(new int[] { 2, 3, 2, 2 }, false)]
        public void Double23_Test(int[] numbers, bool expectedResult)
        {
            Arrays arr = new Arrays();
            Assert.AreEqual(expectedResult, arr.Double23(numbers));
        }
        //Ex.#13
        [TestCase(new int[] {1, 2, 3}, new int[] {1, 2, 0})]
        [TestCase(new int[] {2, 3, 5}, new int[] {2, 0, 5})]
        [TestCase(new int[] {1, 2, 1}, new int[] {1, 2, 1})]
        public void Fix23_Test(int[] numbers, int[] expectedResult)
        {
            Arrays arr = new Arrays();
            Assert.AreEqual(expectedResult, arr.Fix23(numbers));
        }
        //Ex.#14
        [TestCase(new int[] { 1, 3, 4, 5 }, true)]
        [TestCase(new int[] { 2, 1, 3, 4, 5 }, true)]
        [TestCase(new int[] { 1, 1, 1 }, false)]
        public void Unlucky1_Test(int[] numbers, bool expectedResult)
        {
            Arrays arr = new Arrays();
            Assert.AreEqual(expectedResult, arr.Unlucky1(numbers));
        }
        //Ex.#15
        [TestCase(new int[] {4, 5}, new int[] {1, 2, 3}, new int[] {4, 5})]
        [TestCase(new int[] {4}, new int[] {1, 2, 3}, new int[] {4, 1})]
        [TestCase(new int[] {}, new int[] {1, 2}, new int[] {1, 2})]
        public void make2Test(int[] a, int[] b, int[] expectedResult)
        {
            Arrays arr = new Arrays();
            Assert.AreEqual(expectedResult, arr.make2(a,b));
        }
    }
}
