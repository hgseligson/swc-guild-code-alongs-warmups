﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzz
{
    public class Program
    {
        static void Main()
        {
            int number = 0;

            do
            {
                number++;
                Console.Write("{0}",number);
                {
                    if (number % 3 == 0)
                    {
                        Console.Write("Fizz");         
                    }
                    if (number%5 == 0)
                    {
                        Console.Write("Buzz");
                    }

                    Console.Write("\n");
                }

            } while (number < 100);
            //Always remeber to include ReadLine to execute text
            Console.ReadLine();
        }
    }
}
