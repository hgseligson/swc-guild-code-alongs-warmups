﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuessingGame
{
    public class GamePlay
    {
        public bool PlayRound(string name)
        {
            //Determine the number to guess
            Random random = new Random();
            int theAnswer = random.Next(1,21);
            int playerGuess = 0;
            int guesses = 0;

            bool isNumberGuessed = false;
            do
            {
                //Get the player's guess
                Console.Write("{0}. Enter your guess: ", name);
                string playerInput = Console.ReadLine();

                //Need to make sure we got a number from the user
                if (int.TryParse(playerInput, out playerGuess))
                {
                    if (guesses > theAnswer)
                    {
                        Console.WriteLine("{0} your guess is too high!", playerGuess);
                      }
                    else if (guesses < theAnswer)
                    {
                        Console.WriteLine("{0} your guess is too low!", playerGuess);
                    }
                    else 
                    {
                        Console.WriteLine("{0}, YOU FINALLY GOT IT! GO YOU!", name.ToUpper());
                        isNumberGuessed = true;
                    }
                }
            } while (!isNumberGuessed); 

            //Ask User if they want to play again
            bool playAnotherRound = false;
            Console.Write("{0},Would You Like to play Again? (Y/N): ", name);
            string response = Console.ReadLine();

            if (response.ToUpper() == "Y")
            {
                playAnotherRound = true;
            }

            return playAnotherRound;
        }
    }
}
