﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuessingGame
{
    class Program
    {
        static void Main(string[] args)
        {
            //Got the player's  name
            Console.Write("Please Enter Your Name:");
            string name = Console.ReadLine();

            bool keepPlaying = false;
            do
            {
                //create a GamePlay object
                GamePlay game = new GamePlay();
                keepPlaying = game.PlayRound(name);
            } while (keepPlaying);
        }
    }
}
