﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasicInheritance
{
    class Program
    {
        static void Main(string[] args)
        {
            MiniVan myMiniVan = new MiniVan();
            Console.WriteLine("myVan max speed is {0}", myMiniVan.MaxSpeed);

            MiniVan secondVan = new MiniVan(75,0);
            Console.WriteLine("secondVan max speed is {0}", secondVan.MaxSpeed);

            MiniVan thirdVan = new MiniVan(100);
            Console.WriteLine("thirdVan max speed is {0}", thirdVan.MaxSpeed);
            Console.Read();
        }
    }
}
