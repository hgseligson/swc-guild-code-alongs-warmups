﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasicInheritance
{
    //doesn't take any parameters. the colon (:) base is just an int.
    //and 45 is calling in the max from the Car class
    public class MiniVan : Car
    {
        //base is telling us what the max is for the MiniVan (MiniVan is a class of the car (similar to ex.square/rectangle)
        public MiniVan() : base(45) { }

        public MiniVan(int max, int min) : base(max,min) { }

        public MiniVan(int max) { }
    }
}
