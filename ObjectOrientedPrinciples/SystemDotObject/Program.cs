﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystemDotObject
{
    class Program
    {
        static void Main(string[] args)
        {
            Point newPoint = new Point(5,6);
            Console.WriteLine(newPoint);

            Point anotherPoint = new Point(5,6);
            //comparing pointers / not data
            Console.WriteLine(Object.Equals(newPoint, anotherPoint));
            Console.WriteLine(Object.ReferenceEquals(newPoint, anotherPoint));

            Point thirdPoint = newPoint.Copy();
            Console.WriteLine(Object.Equals(newPoint, thirdPoint));
            Console.WriteLine(Object.ReferenceEquals(newPoint, thirdPoint));

            Console.ReadLine();
        }
    }
}
