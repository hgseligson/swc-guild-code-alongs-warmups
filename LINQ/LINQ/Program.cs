﻿using System;
using System.Globalization;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.InteropServices;

namespace LINQ
{
    class Program
    {
        /* Practice your LINQ!
         * You can use the methods in Data Loader to load products, customers, and some sample numbers
         * 
         * NumbersA, NumbersB, and NumbersC contain some ints
         * 
         * The product data is flat, with just product information
         * 
         * The customer data is hierarchical as customers have zero to many orders
         */
        static void Main()
        {
            //PrintOutOfStock();
            //InStockMoreThan3();
            //ex3();
            //ex4();
            //ex5();
            //ex6();
            //ex7();
            //ex8();
            //ex9();
            //ex10();
            //ex11();
            //ex12();
            //First3OrderInWash();
            //ex13();
            //ex14();
            //ex15();
            //ex16();
            //ReturnValuesLessThanPosition();
            //ex17();
            //ex18();
            //ex19();
            //ex20();
            //ex21();
            //ex22();
            //ex23();
            //GroupThemByYearMonth();
            //ex25();
            //ex26();
            //ex27();
            //ex28();
            //ex29();
            //ex30();
            //ex31();
            //ex32();
            //ex34();
            //ex35();
            //ex36();
            //ex37();
            //ex39()
            //AllProductsInCategoryInStock();
            //GetLowestProductInCategory();
            //ex40();



            Console.ReadLine();
        }

        private static void PrintOutOfStock()
        {
            var products = DataLoader.LoadProducts();

            //var results = products.Where(p => p.UnitsInStock == 0);
            var results = from p in products
                          where p.UnitsInStock == 0
                          select p;

            foreach (var product in results)
            {
                Console.WriteLine(product.ProductName);
            }
        }

        //2. Find all products that are in stock and cost more than 3.00 per unit.
        private static void InStockMoreThan3()
        {
            var products = DataLoader.LoadProducts();

            //var results = products.Where(p => p.UnitsInStock > 0 && p.UnitPrice > 3);
            var results = from p in products
                          where p.UnitsInStock > 0 && p.UnitPrice > 3
                          select p;

            foreach (var product in results)
            {
                Console.WriteLine("{0} has {1} in stock with unit price {2}", product.ProductName,
                    product.UnitsInStock, product.UnitPrice);
            }
        }

        private static void ex3()
        {
            var customers = DataLoader.LoadCustomers();
            var query = from c in customers
                        where c.Region == "WA"
                        select c;
            foreach (Customer customer in query)
            {
                Console.WriteLine(customer.CompanyName);
                foreach (Order order in customer.Orders)
                {
                    Console.WriteLine(order.Total);
                }
            }
        }

        private static void ex4()
        {
            var products = DataLoader.LoadProducts();
            foreach (Product product in products)
            {
                Console.WriteLine(products);
            }
        }

        private static void ex5()
        {
            var products = DataLoader.LoadProducts();
            var result = products.Select(p => new { p.ProductName, UnitPrice = p.UnitPrice * 1.25m });

            foreach (var r in result)
            {
                Console.WriteLine("The new price of {0} is {1:C}\n", r.ProductName, r.UnitPrice);
            }

            Console.ReadKey();
        }

        private static void ex6()
        {
            var products = DataLoader.LoadProducts();
            var results = from p in products
                          select p.ProductName.ToUpper();
            foreach (var result in results)
            {
                Console.WriteLine(result);
            }

        }

        private static void ex7()
        {
            var products = DataLoader.LoadProducts();
            var evens = products.Where(p => p.UnitsInStock % 2 == 0);

            foreach (var e in evens)
            {
                Console.WriteLine("{0}\nUnits in stock: {1}\n", e.ProductName, e.UnitsInStock);
            }
            Console.ReadLine();
        }

        private static void ex8()
        {
            var products = DataLoader.LoadProducts();
            var result = products.Select(p => new { p.ProductName, p.Category, Price = p.UnitPrice });

            foreach (var r in result)
            {
                Console.WriteLine("{0}\n{1}\n{2}\n", r.ProductName, r.Category, r.Price);
            }
            Console.ReadLine();
        }
        private static void ex9()
        {
            int[] numbersB = DataLoader.NumbersB;
            int[] numbersC = DataLoader.NumbersC;

            //ex1: var results = from b in numbersB
            //    from c in numbersC
            //    where b < c
            //    select new {b, c};
            //Notes: projecting b into c (=> label syntax | returning a result from 2 collections
            //ex2: var results = numbersB.SelectMany(b => numbersC, (b, c) => new {b, c})
            //    .Where(x => x.b < x.c);

            //ex3: int i = -1;
            //var results = from b in numbersB
            //    let index = i++
            //    where b < numbersC[i]
            //    select new {b = b, c = numbersC[i]};

            var results = numbersB.Select((number, index) => new { b = number, c = numbersC[index] })
                .Where(x => x.b < x.c);

            foreach (var result in results)
            {
                Console.WriteLine("b = {0}, c = {1}", result.b, result.c);
            }
        }

        private static void ex10()
        {
            var customers = DataLoader.LoadCustomers();

            var result = from c in customers
                         from o in c.Orders
                         where o.Total < 500
                         select new { c.CustomerID, o.OrderID, o.Total };

            foreach (var entry in result)
            {
                Console.WriteLine(entry);
            }
            Console.ReadLine();
        }

        private static void ex11()
        {
            var result = DataLoader.NumbersA.Take(3);

            foreach (var r in result)
            {
                Console.WriteLine(r);
            }
            Console.ReadLine();
        }

        //reviewed in class
        private static void First3OrderInWash()
        {
            var customers = DataLoader.LoadCustomers();

            var results = (from c in customers
                           from o in c.Orders
                           where c.Region == "WA"
                           select new { c.CompanyName, o.OrderDate, o.OrderID }).Take(3);

            foreach (var o in results)
            {
                Console.WriteLine("{0} {1} - {2}", o.OrderDate, o.OrderID, o.CompanyName);
            }

        }

        private static void ex12()
        {
            var customers = DataLoader.LoadCustomers();

            var firstThreeOrders = (from c in customers
                                    from o in c.Orders
                                    where c.Region == "WA"
                                    select new { o.OrderID })

                .Take(3);
            foreach (var r in firstThreeOrders)
            {
                Console.WriteLine(r);
            }
            Console.ReadLine();
        }

        private static void ex13()
        {
            var result = DataLoader.NumbersA.Skip(3);

            foreach (var r in result)
            {
                Console.WriteLine(r);
            }
            Console.ReadLine();
        }

        private static void ex14()
        {
            var customers = DataLoader.LoadCustomers();

            var allButFirst2 = (from c in customers
                                from o in c.Orders
                                where c.Region == "WA"
                                select new { o.OrderID })

                .Skip(2);

            foreach (var a in allButFirst2)
            {
                Console.WriteLine(a);
            }

            Console.ReadLine();
        }

        private static void ex15()
        {
            var result = DataLoader.NumbersC.TakeWhile(n => n < 6);

            foreach (var r in result)
            {
                Console.WriteLine(r);
            }

            Console.ReadLine();
        }

        //reviewed in class
        private static void ReturnValuesLessThanPosition()
        {
            var numbersC = DataLoader.NumbersC;

            var results = numbersC.TakeWhile((number, index) => number >= index);

            foreach (var result in results)
            {
                Console.WriteLine(result);
            }
        }

        //did I do this correctly?
        private static void ex16()
        {
            var result = DataLoader.NumbersC.TakeWhile(n => n > Array.IndexOf(DataLoader.NumbersC, n));

            foreach (var r in result)
            {
                Console.WriteLine(r);
            }
            Console.ReadLine();
        }

        private static void ex17()
        {
            var result = DataLoader.NumbersC.SkipWhile(num => num % 3 != 0);

            foreach (var r in result)
            {
                Console.WriteLine(r);
            }
            Console.ReadLine();
        }

        private static void ex18()
        {
            var products = DataLoader.LoadProducts();

            var result = products.OrderBy(p => p.ProductName);

            foreach (var r in result)
            {
                Console.WriteLine(r.ProductName);
            }
            Console.ReadLine();
        }

        private static void ex19()
        {
            var products = DataLoader.LoadProducts();

            var result = products.OrderByDescending(p => p.UnitsInStock);

            foreach (var r in result)
            {
                Console.WriteLine(r.ProductName);
            }
            Console.ReadLine();
        }

        private static void ex20()
        {
            var products = DataLoader.LoadProducts();

            var result = ((products.OrderBy(p => p.Category)).ThenByDescending(p => p.UnitPrice));

            foreach (var r in result)
            {
                Console.WriteLine(r.Category + " " + r.UnitPrice);
            }
            Console.ReadLine();
        }

        private static void ex21()
        {
            var result = DataLoader.NumbersC.Reverse();

            foreach (var r in result)
            {
                Console.WriteLine(r);
            }
            Console.ReadLine();
        }

        private static void ex22()
        {
            var result = DataLoader.NumbersC.OrderBy(n => n%5);

            foreach (var r in result)
            {
                Console.WriteLine(r);
            }
            Console.ReadLine();
        }

        private static void ex23()
        {
            var products = DataLoader.LoadProducts();

            var result = products.GroupBy(p => p.Category);

            foreach (var group in result)
            {
                foreach (var g in group)
                {
                  Console.WriteLine(g.Category + "\t\t" + g.ProductName);  
                }
            }
            Console.ReadLine();
        }
        //ex24: reviewed in class
        private static void GroupThemByYearMonth()
        {
            var customers = DataLoader.LoadCustomers();

            var results = from c in customers
                          select new
                          {
                              companyName = c.CompanyName,
                              YearGroups = from o in c.Orders
                                           group o by o.OrderDate.Year
                                  into yearGroup
                                           select new
                                           {
                                               Year = yearGroup.Key,
                                               monthGroups = from order in yearGroup
                                                             group order by order.OrderDate.Month
                                                   into monthGroup
                                                             select new
                                                             {
                                                                 Month = monthGroup.Key,
                                                                 Orders = monthGroup
                                                             }
                                           }
                          };
            foreach (var result in results)
            {
                Console.WriteLine(result.companyName);

                foreach (var yearGroup in result.YearGroups)
                {
                    Console.WriteLine("\t{0}", yearGroup.Year);

                    foreach (var monthGroup in yearGroup.monthGroups)
                    {
                        Console.Write("\t\t{0}", CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(monthGroup.Month));

                        foreach (var order in monthGroup.Orders)
                        {
                            Console.WriteLine("\t\t\t{0} - {1}", order.OrderDate, order.OrderID);
                        }
                    }
                }
            }
        }

        private static void ex25()
        {
            var products = DataLoader.LoadProducts();

            var result = (from p in products select p.Category).Distinct();

            foreach (var r in result)
            {
                Console.WriteLine(r);
            }
            Console.ReadLine();
        }

        private static void ex26()
        {
            var result = (DataLoader.NumbersA.Union(DataLoader.NumbersB)).Distinct();

            foreach (var r in result)
            {
                Console.WriteLine(r);
            }
            Console.ReadLine();
        }

        private static void ex27()
        {
            var result = (DataLoader.NumbersA.Intersect(DataLoader.NumbersB));

            foreach (var r in result)
            {
                Console.WriteLine(r);
            }
            Console.ReadLine();
        }

        private static void ex28()
        {
            var result = (DataLoader.NumbersA.Except(DataLoader.NumbersB));

            foreach (var r in result)
            {
                Console.WriteLine(r);
            }
            Console.ReadLine();

        }

        private static void ex29()
        {
            var products = DataLoader.LoadProducts();

            var result = (products.First(p => p.ProductID == 12));

            Console.WriteLine(result.ProductName);

            Console.ReadLine();
        }

        private static void ex30()
        {
            var products = DataLoader.LoadProducts();

            var result = (products.Exists(p => p.ProductID == 789));

            Console.WriteLine(result);
            Console.ReadLine();
        }

        private static void ex31()
        {
            var products = DataLoader.LoadProducts();

            var result = from p in products
                         where p.UnitsInStock < 1
                         select p;
            foreach (var r in result)
            {
                Console.WriteLine(r.ProductName);
            }
            Console.ReadLine();
        }

        private static void ex32()
        {
            var result = DataLoader.NumbersB.All(n => n < 9);

            Console.WriteLine(result);

            Console.ReadLine();
        }

        //ex33: reviewed in class
        private static void AllProductsInCategoryInStock()
        {
            var products = DataLoader.LoadProducts();

            var results = from p in products
                          group p by p.Category
                into pCats
                          where pCats.All(x => x.UnitsInStock > 0)
                          select new { pCats.Key, coll = pCats };

            foreach (var result in results)
            {
                Console.WriteLine(result.Key);
                foreach (var product in result.coll)
                {
                    Console.WriteLine("\t{0} - {1}", product.UnitsInStock, product.ProductName);
                }
            }
        }
        //ex.34
        private static void ex34()
        {
            var numA = DataLoader.NumbersA;
            var results = from n in numA
                          select n;
            int count = 0;
            foreach (int result in results)
            {
                int isIt = result % 2;
                if (isIt != 0)
                {
                    count++;
                }
            }
            Console.WriteLine(count);

        }

        //ex.35
        private static void ex35()
        {
            var cus = DataLoader.LoadCustomers();
            var results = from c in cus
                          from o in c.Orders
                          select new
                          {
                              cusID = c.CustomerID,
                              orderNum = c.Orders.Length
                          };


            foreach (var result in results)
            {

                Console.WriteLine(result.cusID + " - " + result.orderNum);
            }
        }

        //ex.36
        private static void ex36()
        {
            var products = DataLoader.LoadProducts();
            var results = products.GroupBy(c => c.Category);

            foreach (var result in results)
            {
                Console.WriteLine(result.Key + " - " + result.Key.Length);
            }
        }

        //ex.37
        private static void ex37()
        {
            var products = DataLoader.LoadProducts();
            var results = products.GroupBy(p => p.Category, u => u.UnitsInStock);

            foreach (var result in results)
            {
                Console.WriteLine(result.Key + " - " + result.Key.Length);
            }

        }

        //ex.38
        private static void ex38()
        {
            var products = DataLoader.LoadProducts();
            var results = from p in products
                          group p by p.Category
                into pCats
                          select new
                          {
                              pCats.Key,
                              minprod = pCats.Min(prod => prod.UnitPrice),
                              prodName = pCats.First(prod => prod.UnitPrice == pCats.Min(p => p.UnitPrice))
                          };

            foreach (var result in results)
            {
                Console.WriteLine("{0} - {1:C} {2}", result.Key, result.minprod, result.prodName.ProductName);
            }
        }

        //ex.39
        private static void ex39()
        {
            var products = DataLoader.LoadProducts();
            var results = from p in products
                          group p by p.Category
                into pCats
                          select new
                          {
                              pCats.Key,
                              maxprod = pCats.Max(prod => prod.UnitPrice),
                              prodName = pCats.First(prod => prod.UnitPrice == pCats.Max(p => p.UnitPrice))
                          };

            foreach (var result in results)
            {
                Console.WriteLine("{0} - {1:C} {2}", result.Key, result.maxprod, result.prodName.ProductName);
            }
        }

        //ex38: reviewed in class
        private static void GetLowestProductInCategory()
        {
            var products = DataLoader.LoadProducts();

            var results = from p in products
                          group p by p.Category
                into pCats
                          select new
                          {
                              pCats.Key,
                              minprod = pCats.Min(prod => prod.UnitPrice),
                              prodName = pCats.First(prod => prod.UnitPrice == pCats.Min(p => p.UnitPrice))
                          };
            //var results = products.GroupBy(prod => prod.Category)
            //    .Select(x => new
            //    {
            //        x.Key,
            //        miniprod = x.Min(p => p.UnitPrice),
            //        prodName = x.First(prod => prod.UnitPrice == x.Min(p => p.UnitPrice))
            //    });

            foreach (var result in results)
            {
                Console.WriteLine("{0} - {1:C} {2}", result.Key, result.minprod, result.prodName.ProductName);
            }
        }

        //ex.40
        private static void ex40()
        {
            var products = DataLoader.LoadProducts();
            var results = from p in products
                          group p by p.Category
                into pCats
                          select new
                          {
                              pCats.Key,
                              aveprod = pCats.Average(prod => prod.UnitPrice)

                          };
            foreach (var result in results)
            {
                Console.WriteLine("{0} - {1:C}", result.Key, result.aveprod);
            }

            }
        }
    }




