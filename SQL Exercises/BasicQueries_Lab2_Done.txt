PDF | Pg.14
Lab#2
Lab Exercises
From the Orders table, write these queries:
1. Select all the order information for the
customer QUEDE.

SELECT *
FROM Customers
WHERE CustomerID = 'QUEDE'

2. Select the orders whose freight is more than
$100.00.

SELECT *
FROM ORDERS
WHERE Freight > 100.00

3. Select the orders shipping to the USA whose
freight is between $10 and $20.

SELECT *
FROM ORDERS
WHERE ShipCountry = 'USA' AND Freight BETWEEN 10 AND 20