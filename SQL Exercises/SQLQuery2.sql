CREATE TABLE [dbo].[Movies](
[MovieID] [int] IDENTITY (1,1) NOT NULL PRIMARY KEY,
[MovieTitle] [varchar] (30) NOT NULL, 
[RunningTime] [int] NULL, 
[Rating] [varchar] (30) NULL
)
