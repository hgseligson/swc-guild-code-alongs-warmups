-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE OrderDetailsByDateRange
	-- Add the parameters for the stored procedure here
	@BeginDate datetime,
	@EndDate datetime,
	@Count int output
AS
BEGIN
	SELECT o.OrderID, o.OrderDate, p.ProductName, p.UnitPrice
		FROM [Order Details] od
			INNER JOIN Orders o on o.OrderID = od.OrderID
			INNER JOIN Products p on p.ProductID = od.ProductID
		WHERE o.OrderDate BETWEEN @BeginDate AND @EndDate

	SELECT @Count = COUNT(*) FROM [Order Details] od
			INNER JOIN Orders o on o.OrderID = od.OrderID
			INNER JOIN Products p on p.ProductID = od.ProductID
		WHERE o.OrderDate BETWEEN @BeginDate AND @EndDate
END
GO
