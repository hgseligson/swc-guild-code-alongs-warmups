SELECT * FROM [Order Details]

SELECT * FROM Products

SELECT o.OrderID, o.OrderDate, p.ProductName, p.UnitPrice
	FROM [Order Details] od
	INNER JOIN Orders o on o.OrderID = od.OrderID
	INNER JOIN Products p on p.ProductID = od.ProductID
	WHERE o.OrderDate BETWEEN '1/01/1997' AND '1/15/1997'

DECLARE @RowCount int 
EXEC OrderDetailsByDateRange '1/01/1997', '1/15/1997', @RowCount output

SELECT @RowCount