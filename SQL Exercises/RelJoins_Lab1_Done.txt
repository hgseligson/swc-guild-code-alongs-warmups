PDG | Pg.18
Lab #1
Lab Exercises
� Get a list of each employee and their
territories.

SELECT *
FROM Employees e
INNER JOIN EmployeeTerritories et ON e.EmployeeID = et.EmployeeID
INNER JOIN Territories t ON et.TerritoryID = t.TerritoryID

� Get the Customer Name, Order Date, and
each order detail�s Product name for USA
customers only.

SELECT c.ContactName,OrderDate,ProductName, c.Country FROM Customers c
INNER JOIN Orders o ON o.CustomerID = c.CustomerID
INNER JOIN [Order Details] od ON  o.OrderID = od.OrderID
INNER JOIN [Products] p ON p.ProductID = od.ProductID
WHERE c.Country = 'USA'

� Get all the order information where Chai was
sold.

SELECT o.OrderID, c.CompanyName, o.OrderDate, p.ProductName, c.Country
FROM Orders o
INNER JOIN customers c
ON o.CustomerID = c.CustomerID
INNER JOIN [Order Details] od
ON o.OrderID = od.OrderID
inner join Products p
ON od.ProductID = p.ProductID
WHERE c.Country = 'USA' and p.ProductName = 'Chai'









